<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => Carbon::now(),
                'Interview_Summary' => 'The interview was successful,
                The candidate impressed me greatly',
            ],
            [
                'date' => Carbon::now(),
                'Interview_Summary' => 'The candidate was fine, he has a lot of experience in the field'
            ],                      
            ]);            

    }
}
