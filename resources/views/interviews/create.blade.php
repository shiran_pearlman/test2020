@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "interview_summary">interview summary</label>
            <input type = "text" class="form-control" name = "interview_summary">
        </div>     
        <div class="form-group">
            <label for = "date">date</label>
            <input type = "date" class="form-control" name = "date">
        </div> 
        <div>
            <label for = "candidate_id">Assign candidate</label>
            <select class="form-control" name="candidate_id">
                @foreach($candidates as $candidate)
                    <option value="{{$candidate->id}}">{{$candidate->name}}</option>
                @endforeach
            </select>
        </div> 
        <div>
            <label for = "user_id">Assign user</label>
            <select class="form-control" name="user_id">
                @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
            </select>
        </div> 

        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
