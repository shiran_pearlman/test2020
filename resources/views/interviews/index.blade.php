@extends('layouts.app')

@section('title', 'Candidates')

@section('content')

<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>interviews</h1>
@if($interviews->isEmpty())
You have no interviews

@else
<table class = "table table-dark">
    <tr>
        <th>id</th>
        <th>Interview Summary</th>
        <th>date</th>
        <th>candidate</th>
        <th>user</th>

    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->Interview_Summary}}</td>
            <td>{{$interview->date}}</td>
            <td>@if(isset($interview->candidate_id))
                          {{$interview->candidate->name}}  
                        @else
                          no candidate
                        @endif</td>
            <td>@if(isset($interview->user_id))
                        {{$interview->user->name}}  
                        @else
                            no user
                        @endif</td>
        </tr>
    @endforeach
                
</table>
@endif
@endsection

                

